#!/usr/bin/env python3
from install import execute, execute_with_pipe, read_input, insert_doc, insert_doc_arm
from install import get_host_name, get_host_ip


arch = read_input("Please enter your architecture(x86/arm)[x86]", "x86")

while arch != 'x86' and arch != 'arm':
    print("Please enter x86 or arm")
    arch = read_input("Please enter your architecture(x86/arm)[x86]", "x86")
    if arch == 'x86' or arch == 'arm':
        break

if arch == 'arm':
    # Start mongo container
    if (execute_with_pipe("docker ps -a", "grep DORRY-ARM-platform-db") != None):
        execute("docker rm -f DORRY-ARM-platform-db")
    execute(
        "docker run -d -p 27117:27017 --restart=always --name DORRY-ARM-platform-db dhermanns/rpi-mongo")

    # Start Dorry Platform Backend container
    host_name = get_host_name()
    market_url = read_input(
        "Please enter the url of Dorry Market[http://dorry.market]", "http://dorry.market")
    back_ver = read_input(
        "Please enter the version of Dorry Platform Backend[latest]", "latest")
    front_ver = read_input(
        "Please enter the version of Dorry Platform Frontend[latest]", "latest")
    if execute_with_pipe("docker ps -a", "grep DORRY-ARM-platform-backend") != None:
        execute("docker stop DORRY-ARM-platform-backend")
        execute("docker rm DORRY-ARM-platform-backend")
    execute("docker run -d --net=host -e REGISTRY_HOST={} -v /var/run/docker.sock:/var/run/docker.sock --name DORRY-ARM-platform-backend dorrydoufu/dorry-arm-platform-backend:{}".format(market_url, back_ver))

    # Start Dorry Platform Frontend container
    if execute_with_pipe("docker ps -a", "grep DORRY-ARM-platform-frontend") != None:
        execute("docker stop DORRY-ARM-platform-frontend")
        execute("docker rm DORRY-ARM-platform-frontend")
    execute("docker run -itd -p 4200:4200 -e DORRY_PLATFORM_BACKEND={} --name DORRY-ARM-platform-frontend dorrydoufu/dorry-arm-platform-frontend:{}".format(host_name, front_ver))

    # Register an user
    username = read_input("Please enter the username(default: dorry)", "dorry")
    password = read_input(
        "Please enter the password(default: abc123_)", "abc123_")
    print(insert_doc_arm({"username": username, "password": password}))

else:
    # Start mongo container
    if (execute_with_pipe("docker ps -f status=exited", "grep dorry-platform-db") != None
            or execute_with_pipe("docker ps -f status=created", "grep dorry-platform-db") != None):
        execute("docker rm dorry-platform-db")
        execute(
            "docker run -itd -p 27000:27017 --restart=always --name dorry-platform-db mongo")
    elif execute_with_pipe("docker ps", "grep dorry-platform-db") != None:
        pass
    else:
        execute(
            "docker run -itd -p 27000:27017 --restart=always --name dorry-platform-db mongo")

    host_name = read_input(
        "Please enter the host name(default: " + get_host_name() + ")", get_host_name())
    host_ip = read_input(
        "Please enter the host ip(default: " + get_host_ip() + ")", get_host_ip())
    kube_api = "http://" + host_name + ":8080"
    market_url = read_input(
        "Please enter the url of Dorry Market(default: dorry.market)", "dorry.market")
    back_ver = read_input(
        "Please enter the version of Dorry Platform Backend(default: latest)", "latest")
    front_ver = read_input(
        "Please enter the version of Dorry Platform Frontend(default: latest)", "latest")

    # Start Dorry Platform Backend container
    if execute_with_pipe("docker ps -a", "grep dorry-platform-backend") != None:
        execute("docker stop dorry-platform-backend")
        execute("docker rm dorry-platform-backend")
    execute("docker run -itd --restart=always --network=host -e KUBEAPI={} -e MARKET_URL={} -e DATABASE={} -e DATABASE_PORT={} -e HOST_IP={} --name dorry-platform-backend-{} -p 12000:12000 --privileged -v /var/run/docker.sock:/var/run/docker.sock dorrydoufu/dorry-platform-backend:{}".format(kube_api, market_url, host_name, 27000, host_ip, back_ver, back_ver))

    # Start Dorry Platform Frontend container
    if execute_with_pipe("docker ps -a", "grep dorry-platform-frontend") != None:
        execute("docker stop dorry-platform-frontend")
        execute("docker rm dorry-platform-frontend")
    execute("docker run -itd --network=host --restart=always --name dorry-platform-frontend-{} -e KUBE_API={} -e FRONTEND_VERSION={} -e BACKEND_VERSION={} -p 4200:4200 dorrydoufu/dorry-platform-frontend:{}".format(front_ver, host_name, front_ver, back_ver, front_ver))

    # Register an user
    username = read_input("Please enter the username(default: dorry)", "dorry")
    password = read_input(
        "Please enter the password(default: abc123_)", "abc123_")
    print(insert_doc({"username": username, "password": password}))
