#!/usr/bin/env python3
import pymongo
import json


# Connect to database
client = pymongo.MongoClient('localhost', 28000)
db = client.dorry

# Delete old data
del_app_res = db.applications.delete_many({})
del_repo_res = db.repository.delete_many({})
if del_app_res and del_repo_res:
    print('Database has been cleaned.')

# Initialize market database
with open('application.json', 'rt') as f:
    app_list = json.loads(f.read())
for app in app_list:
    insert_app_res = db.applications.insert_one(app)
    repo_keyset = ('applicationId', 'name', 'version', 'description',
                   'author', 'pictureUrl')
    repo = {key: app[key] for key in repo_keyset}
    insert_repo_res = db.repository.insert_one(repo)
    if insert_app_res == None or insert_repo_res == None:
        print('Failed to insert data on {}'.format(app['applicationId']))
print('Database has been initialized.')
