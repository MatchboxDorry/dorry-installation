#!/usr/bin/env python3
from install import execute, execute_with_pipe


# Install Docker
execute("apt-get install -y docker.io")

# Install Kubernetes
execute("apt-get install -y curl")
execute_with_pipe(
    "curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg", "apt-key add -")
execute("echo 'deb http://apt.kubernetes.io/ kubernetes-xenial main' > /etc/apt/sources.list.d/kubernetes.list")
execute("apt-get update")
execute("apt-get install -y kubelet kubeadm kubectl kubernetes-cni")

# Initialize Kubernetes
execute("kubeadm reset")
execute("kubeadm init")
execute("kubectl taint nodes --all dedicated-")
execute("kubectl apply -f calico.yaml")
execute("kubectl create namespace dorry-system")

execute("sed -i 's/--insecure-bind-address=127.0.0.1/--insecure-bind-address=0.0.0.0/g' /etc/kubernetes/manifests/kube-apiserver.json")
execute("systemctl restart kubelet.service")
