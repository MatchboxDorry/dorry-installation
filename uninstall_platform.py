#!/usr/bin/env python3
from install import execute, execute_with_pipe, read_input


arch = read_input("Please enter your architecture(x86/arm)[x86]", "x86")

while arch != 'x86' and arch != 'arm':
    print("Please enter x86 or arm")
    arch = read_input("Please enter your architecture(x86/arm)[x86]", "x86")
    if arch == 'x86' or arch == 'arm':
        break

back_ver = read_input(
    "Please enter the version of Dorry Platform Backend", "latest")
front_ver = read_input(
    "Please enter the version of Dorry Platform Frontend", "latest")

if arch == 'arm':
    if execute_with_pipe("docker ps -a", "grep DORRY-ARM-platform-db") != None:
        execute("docker rm -f -v DORRY-ARM-platform-db")
    if execute_with_pipe("docker ps -a", "grep DORRY-ARM-platform-backend") != None:
        execute("docker rm -f -v DORRY-ARM-platform-backend")
    if execute_with_pipe("docker ps -a", "grep DORRY-ARM-platform-frontend") != None:
        execute("docker rm -f -v DORRY-ARM-platform-frontend")
    if execute_with_pipe("docker images", "grep DORRY-ARM-platform-frontend") != None:
        execute(
            "docker rmi dorrydoufu/DORRY-ARM-platform-frontend:{}".format(front_ver))
    if execute_with_pipe("docker images", "grep DORRY-ARM-platform-frontend") != None:
        execute("docker rmi dorrydoufu/DORRY-ARM-platform-backend:{}".format(back_ver))

else:
    if execute_with_pipe("docker ps -a", "grep dorry-platform-db") != None:
        execute("docker stop dorry-platform-db")
        execute("docker rm -v dorry-platform-db")
    if execute_with_pipe("docker ps -a", "grep dorry-platform-backend") != None:
        execute("docker stop dorry-platform-backend-{}".format(back_ver))
        execute("docker rm -v dorry-platform-backend-{}".format(back_ver))
    if execute_with_pipe("docker ps -a", "grep dorry-platform-frontend") != None:
        execute("docker stop dorry-platform-frontend-{}".format(front_ver))
        execute("docker rm -v dorry-platform-frontend".format(front_ver))

    if execute_with_pipe("docker images", "grep dorry-platform-frontend") != None:
        execute("docker rmi dorrydoufu/dorry-platform-frontend:{}".format(front_ver))
    if execute_with_pipe("docker images", "grep dorry-platform-frontend") != None:
        execute("docker rmi dorrydoufu/dorry-platform-backend:{}".format(back_ver))
    