#!/usr/bin/env python3
from install import execute, execute_with_pipe, read_input
import subprocess
import getopt
import os
import sys

backend=""
frontend=""
kubeapi = ""
marketurl = ""
database = ""
databaseport = ""
hostip = ""
backendapi = ""

def usage():
    print ("UPDATE DORRY PLATFORM OR MARKET")
    print ("                         Author:Terry")
    print ("                         Version:0.1.0")
    print ("                         Date:2017-3-13")
    print
    print ("Usage:")
    print ("       ./update.py -b 0.1.2 -f 0.2.1")
    print
    print ("    -h,--help           -show the help content.")
    print ("    -b                  -specific dorry-platform-backend version.")
    print ("    -f                  -specific dorry-platform-frontend version.")
    print
    print
    print
    print
    sys.exit(1)

def getOldBackVersion():
    proc = subprocess.Popen("docker ps -a | grep dorry-platform-backend | awk '{print $NF}' | awk -F '-' '{print $NF}' | head -n 1",shell=True,stdout=subprocess.PIPE)
    version = proc.stdout.read().decode(encoding="UTF-8").rstrip()
    print(version)
    return version

def getOldFrontVersion():
    proc = subprocess.Popen("docker ps -a | grep dorry-platform-frontend | awk '{print $NF}' | awk -F '-' '{print $NF}' | head -n 1",shell=True,stdout=subprocess.PIPE)
    version = proc.stdout.read().decode(encoding="UTF-8").rstrip()
    print(version)
    return version

def getBackendEnv(env):
    #return execute("docker exec dorry-platform-backend bash -c 'echo \"$" + env + "\"'")
    proc = subprocess.Popen("docker exec dorry-platform-backend-{} bash -c 'echo ${}'".format(getOldBackVersion(),env),shell=True,stdout=subprocess.PIPE)
    output = proc.stdout.read().decode(encoding="UTF-8").rstrip()
    print (output)
    return output

def getFrontendEnv(env):
    #return execute("docker exec dorry-platform-frontend sh -c 'echo \"$" + env + "\"'")
    proc = subprocess.Popen("docker exec dorry-platform-frontend-{} sh -c 'echo ${}'".format(getOldFrontVersion(),env),shell=True,stdout=subprocess.PIPE)
    output = proc.stdout.read().decode(encoding="UTF-8").rstrip()
    print (output)
    return output


def main():
    global oldbackend
    global oldfrontend
    global backend
    global frontend
    global kubeapi
    global marketurl
    global database
    global databaseport
    global hostip
    global backendapi

    if not len(sys.argv[1:]):
        usage()

    try:
        opts,args = getopt.getopt(sys.argv[1:],'hb:f:',
                                  ['help'])
    except getopt.GetoptError as e:
        print (str(e))
        usage()

    if not opts:
        usage()

    temp_opt_list = []
    for opt,arg in opts:
        temp_opt_list.append(opt)

    if ('-b' not in temp_opt_list) or ('-f' not in temp_opt_list):
        print("\x1b[1;%dm" % (31) + 'ERR: Please checkout the paramters!!!' + "\x1b[0m")
        usage()

    for o,a in opts:
        if o in ('-h','--help'):
            usage()
        elif o in ('-b'):
            backend = str(a)
        elif o in ('-f'):
            frontend = str(a)
        else:
            assert False,'Unhandled Option.'

    try:
        execute("docker ps -a | grep dorry-platform-backend")
    except Exception:
        print("\x1b[1;%dm" % (31) + 'ERR: You should use deploy dorry-platform-backend script !!!' + "\x1b[0m")
        return

    try:
        execute("docker ps -a | grep dorry-platform-frontend")
    except Exception:
        print("\x1b[1;%dm" % (31) + 'ERR: You should use deploy dorry-platform-frontend script !!!' + "\x1b[0m")
        return

    if backend != "":
        oldbackend = getOldBackVersion()
        if (backend == oldbackend):
            print("\x1b[1;%dm" % (32) + 'Dorry Platform Backend already the newest version!!!' + "\x1b[0m")
            
        else :
            execute("docker restart -t 5 $(docker ps -a | grep dorry-platform-backend | awk '{print $1}')")

            # envirionment variables
            kubeapi = getBackendEnv("KUBEAPI")
            marketurl = getBackendEnv("MARKET_URL")
            database = getBackendEnv("DATABASE")
            databaseport = getBackendEnv("DATABASE_PORT")
            hostip = getBackendEnv("HOST_IP")
            print(kubeapi,marketurl,database,databaseport,hostip)

            execute("docker pull dorrydoufu/dorry-platform-backend:"+backend)
            execute("docker stop $(docker ps -a | grep dorry-platform-backend | awk '{print $1}')")
            execute("docker run -itd --restart=always --network=host -e KUBEAPI={} -e MARKET_URL={} -e DATABASE={} -e DATABASE_PORT={} -e HOST_IP={} --name dorry-platform-backend-{} -p 12000:12000 --privileged -v /var/run/docker.sock:/var/run/docker.sock dorrydoufu/dorry-platform-backend:{}".format(kubeapi,marketurl,database,databaseport,hostip,backend,backend))
            execute("docker rm -f $(docker ps -a | grep -E 'dorry-platform-backend-{}' | awk '{print $1}')".replace('{}',oldbackend))
            # execute("docker rmi -f $(docker images | grep dorry-platform-backend | awk '{print $3}')")


    if frontend != "":
        oldfrontend = getOldFrontVersion()
        if(frontend == oldfrontend):
            print("\x1b[1;%dm" % (32) + 'Dorry Platform Frontend already the newest version!!!' + "\x1b[0m")
        else:    
            execute("docker restart -t 5 $(docker ps -a | grep dorry-platform-frontend | awk '{print $1}')")

            # envirionment variables
            backendapi = getFrontendEnv("KUBE_API")

            execute("docker pull dorrydoufu/dorry-platform-frontend:"+frontend)
            execute("docker stop $(docker ps -a | grep dorry-platform-frontend | awk '{print $1}')")
            execute("docker run -itd --network=host --restart=always --name dorry-platform-frontend-{} -e KUBE_API={} -e FRONTEND_VERSION={} -e BACKEND_VERSION={} -p 4200:4200 dorrydoufu/dorry-platform-frontend:{}".format(frontend,backendapi, frontend, backend, frontend))
            execute("docker rm -f $(docker ps -a | grep dorry-platform-frontend-{} | awk '{print $1}')".replace('{}',oldfrontend))
            # execute("docker rmi $(docker images | grep dorry-platform-frontend | awk '{print $3}')")


if(__name__ == "__main__"):
    main()
