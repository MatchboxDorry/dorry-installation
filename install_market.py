#!/usr/bin/env python3
from install import execute, execute_with_pipe, read_input


# Start mongo container
start_db = read_input("Do you want to start a new database? (y/n)[n]", "n")

while start_db != 'y' and start_db != 'n':
    print("Please enter y or n")
    start_db = read_input(
        "Do you want to start a new database? (y/n)[n]", "n")
    if start_db == 'y' or start_db == 'n':
        break

if start_db == 'y':
    if execute_with_pipe("docker ps -a", "grep dorry-market-db") != None:
        execute("docker rm dorry-market-db")
    execute("docker run -p 28000:27017 --name dorry-market-db "
            "--restart=always -d mongo")

if execute_with_pipe("docker ps -f status=exited", "grep dorry-market-db") != None:
    execute("docker rm dorry-market-db")
    execute("docker run -p 28000:27017 --name dorry-market-db --restart=always -d mongo")
elif execute_with_pipe("docker ps", "grep dorry-market-db") != None:
    pass
else:
    execute("docker run -p 28000:27017 --name dorry-market-db --restart=always -d mongo")

market_ver = read_input(
    "Please enter the version of Dorry Market(default: latest)", "latest")
if execute_with_pipe("docker ps -a", "grep dorry-market$") != None:
    execute("docker stop dorry-market")
    execute("docker rm dorry-market")
execute("docker run --restart=always --privileged --network=host -p 15000:15000 -e MARKET_VERSION={} --name dorry-market -d dorrydoufu/dorry-market:{}".format(market_ver, market_ver))
