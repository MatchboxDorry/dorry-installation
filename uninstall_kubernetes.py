#!/usr/bin/env python3
from install import execute, execute_with_pipe


# Uninstall Kubernetes
execute("kubeadm reset")
execute("apt-get purge -y kubelet kubeadm kubectl kubernetes-cni")

# Remove Docker containers created by Kubernetes
if execute_with_pipe("docker ps -a", "grep k8s") != None:
    execute("docker rm -v $(docker ps -a | grep k8s | awk '{print $1}')")
if execute_with_pipe("docker images", "grep 'gcr.io'") != None:
    execute(
        "docker rmi $(docker images | grep -E 'gcr.io|calico' | awk '{print $3}')")
