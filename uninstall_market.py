#!/usr/bin/env python3
from install import execute, execute_with_pipe, read_input


market_ver = read_input("Please enter the version of Dorry Market", "latest")
remove_db = read_input("Do you want to remove the database? (y/n)[n]", "n")

while remove_db != 'y' and remove_db != 'n':
    print("Please enter y or n")
    remove_db = read_input(
        "Do you want to remove the database? (y/n)[n]", "n")
    if remove_db == 'y' or remove_db == 'n':
        break

if remove_db == 'y':
    execute("docker stop dorry-market-db")
    execute("docker rm dorry-market-db")

if execute_with_pipe("docker ps -a", "grep dorry-market$") != None:
    execute("docker stop dorry-market")
    execute("docker rm dorry-market")

if execute_with_pipe("docker images", "grep dorry-market") != None:
    execute("docker rmi dorrydoufu/dorry-market:{}".format(market_ver))
