import subprocess
import sys
import socket
import pymongo
import shlex
# import plumbum


def execute(cmd):
    try:
        ps = subprocess.run(cmd, shell=True, check=True)
        if ps.stdout:
            return ps.stdout.read().decode(encoding="UTF-8").rstrip()
        return None
    except Exception as e:
        raise e


def execute_with_pipe(*cmds):
    if len(cmds) > 1:
        i, ps = 0, {}
        for cmd in cmds:
            cmd = cmd.strip()
            if i == 0:
                ps[i] = subprocess.Popen(shlex.split(cmd), stdin=None,
                                         stdout=subprocess.PIPE,
                                         stderr=subprocess.PIPE)
            else:
                ps[i] = subprocess.Popen(shlex.split(cmd), stdin=ps[i - 1].stdout,
                                         stdout=subprocess.PIPE,
                                         stderr=subprocess.PIPE)
            exit_code = ps[i].wait()
            if exit_code > 1:
                raise Exception(ps[i].stderr.read().decode(encoding="UTF-8"))
            else:
                pass
            i = i + 1
        out, err = ps[i - 1].communicate()
        if out:
            print(out.decode(encoding="UTF-8").rstrip())
            return out.decode(encoding="UTF-8").rstrip()
        else:
            return None
    else:
        raise Exception("Multiple commands required")


def read_input(prompt, default):
    print(prompt, ":", sep="", end="\n")
    input = sys.stdin.readline().rstrip()
    if input == "":
        input = default
    return input


def insert_doc(doc):
    client = pymongo.MongoClient('localhost', 27000)
    db = client.dorry
    user_id = db.users.insert_one(doc).inserted_id
    if user_id:
        return user_id
    else:
        raise Exception("Insert failed")


def insert_doc_arm(doc):
    client = pymongo.MongoClient('localhost', 27117)
    db = client.userdata
    user_id = db.usermodels.insert_one(doc).inserted_id
    if user_id:
        return user_id
    else:
        raise Exception("Insert failed")


def get_host_name():
    return socket.gethostname()


def get_host_ip():
    return socket.gethostbyname(socket.gethostname())
