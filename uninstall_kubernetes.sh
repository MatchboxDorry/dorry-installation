#!/bin/bash

kubeadm reset
sudo apt-get purge -y kubelet kubeadm kubectl kubernetes-cni

sudo docker rm -f -v $(docker ps -a | grep k8s | awk '{print $1}')

sudo rm -r /mnt/platform/

if [ $(docker images | grep -E 'gcr.io|calico' | awk '{print $3}' | tr -d '[:space:]') = '' ]
then
    echo -e '\033[0;33m>>> No Kubernetes Containers! <<<\033[0m'
else
    sudo docker rmi $(docker images | grep -E 'gcr.io|calico' | awk '{print $3}')
    echo -e '\033[0;33m>>> Removed Kubernetes Containers! <<<\033[0m'
fi

