#!/usr/bin/env python3
from install import execute, execute_with_pipe, read_input

import getopt
import os
import sys

market=""

def usage():
    print ("UPDATE DORRY PLATFORM OR MARKET")
    print ("                         Version:0.1.0")
    print ("                         Date:2017-3-13")
    print
    print ("Usage: ./update.py -m 0.0.1")
    print
    print ("    -h,--help           -show the help content.")
    print ("    -m                  -specific dorry-market version.")
    print
    print
    print
    print
    sys.exit(1)

def main():
    global market

    if not len(sys.argv[1:]):
        usage()

    try:
        opts,args = getopt.getopt(sys.argv[1:],'m:',
                                  ['help'])
    except getopt.GetoptError as e:
        print (str(e))
        usage()

    if not opts:
        usage()
    
    for o,a in opts:
        if o in ('-h','--help'):
            usage()
        elif o in ('-m'):
            market = str(a)
            print (market)
        else:
            useage()

    try:
        execute("docker ps -a | grep dorry-market$")
    except Exception:
        print("ERR: You should use deploy dorry-market script !!!")
        return

    if market != "":
        execute("docker rm -f $(docker ps -a | grep dorry-market | awk '{print $1}')")
        # execute("docker rmi $(docker images | grep dorry-market | awk '{print $3}')")
        execute("docker pull dorrydoufu/dorry-market:"+market)
        execute("docker run -itd --restart=always -p 15000:15000 -e MARKET_VERSION={} --name dorry-market dorrydoufu/dorry-market:{}".format(market, market))
        print (market)

if(__name__ == "__main__"):
    main()
