# dorry-installation
Deploy and clean dorry market and dorry platform.

## Dorry Platform and Market
Firstly, you need to download the script.
```bash
$ git clone https://bitbucket.org/MatchboxDorry/dorry-installation.git
$ cd dorry-installation/
```

You need to enter your bitbucket username and password. If you have no permission, please [email us](mailto:terry.li@dorry.io).


---------------------------------


### Dorry Market
#### Deploy dorry market
1.Prepare.
```bash
$ sudo apt-get install python3-pip
$ sudo pip3 install pymongo
```

2.Install dorry market
```bash
$ sudo ./install_market.py
$ Please enter the version of Dorry Market: > [dorry market tag]
```

###### Deploy Market Options
During deploying dorry market, you need to configure the following attributes:

* **Version of Dorry Market**, the version of image ```dorrydoufu/dorry-market```, *latest* by default.View all versions of [dorrydoufu/dorry-market](https://hub.docker.com/r/dorrydoufu/dorry-market/tags/).

#### Test
Run ```./test_install_market.sh```. If there are containers called ```dorry-market```, ```dorry-market-db``` and their statuses are both **UP**, that means dorry market has been installed successfully.


#### Uninstall dorry market
If you want to uninstall dorry market, please run
```bash
$ sudo ./uninstall_market.py
```

You need to enter the ```version``` of ```dorrydoufu/dorry-market``` image when uninstalling the dorry market.

#### Test
Run ```./test_uninstall_market.sh```. If there are no containers called ```dorry-market```, ```dorry-market-db```,  that means dorry market has been uninstalled successfully.


#### Upgrade dorry market
If you want to upgrade dorry market, please run
```bash
$ sudo ./upgrade_market.py -m [your new market version]
```

Run next command for help:
```bash
$ sudo ./upgrade_market.py -h
```

After upgrade dorry market, if dorry market version changed on dorry-platform page, that means dorry market has been installed successfully.


-------------------------


### Dorry Platform
#### Deploy dorry platform
Install kubernetes


If you are running on x86, please install kubernetes:
```bash
$ source ./install_kubernetes_1.6.sh
```


If you encounter _locale.Error: unsupported locale setting_ when running pip, please set locale environments by:
```
$ export LC_ALL=C
```

#### Test
After script has been finished, run ```./test_install_kubernetes.sh```. If there is version info, kubernetes has been installed successfully.

3.Deploy dorry platform

```bash
$ sudo ./install_platform.py
```

###### Deploy Platform Options
During deploying dorry platform, you need to configure the following attributes:

* **architecture**, it is your computer architecture. Default value is x86.

* **host name**, it is your computer hostname by default. Default value is sufficient.

* **host ip**, this is the ip address of the computer where you deploy dorry platform on. If you don't know your ip, run ```nslookup [your hostname]```.

* **Market Url**, it is the market you choose to connect. e.g, http://dorrymarket:15000

* **Version of Dorry Platform Backend**, version of image ```dorrydoufu/dorry-platform-backend```, *latest* by default. If you are running on x86, check [dorry-platform-backend](https://hub.docker.com/r/dorrydoufu/dorry-platform-backend/tags/) for version information. If you are running on arm, check [dorry-arm-platform-backend](https://hub.docker.com/r/dorrydoufu/dorry-arm-platform-backend/tags/) for version information.

* **Version of Dorry Platform Frontend**, version of image ```dorrydoufu/dorry-platform-frontend```, *latest* by default. If you are running on x86, check [dorry-platform-frontend](https://hub.docker.com/r/dorrydoufu/dorry-platform-frontend/tags/) for version information. If you are running on arm, check [dorry-arm-platform-frontend](https://hub.docker.com/r/dorrydoufu/dorry-arm-platform-frontend/tags/) for version information.

* **Username**, username of dorry platform.  

* **Password**, password of dorry platform.

#### Test
After script has been finished, open browser with url ```http://[yourhostname]:4200``` and login dorry platform with your account.


#### Uninstall dorry platform
Perform the following steps:

1.Uninstall kubernetes, the kubernetes images and containers
```bash
$ sudo ./uninstall_kubernetes.sh
```

#### Test
After script has been finished, run ```./test_uninstall_kubernetes.sh```. If there are no images, kubernetes has been uninstalled successfully.

2.Uninstall the platform containers and images
```bash
$ sudo ./uninstall_platform.py
```

###### Uninstall Platform Options

* **dorry-platform-backend version**, this is version of ```dorrydoufu/dorry-platform-backend```.

* **dorry-platform-frontend version**, this is version of ```dorrydoufu/dorry-platform-frontend```.

#### Test
After script has been finished, run ```./test_uninstall_platform.sh```. If there are no containers, platform has been uninstalled successfully.

#### Upgrade dorry platform
If you want to upgrade dorry market, please run
```bash
$ sudo ./upgrade_platform.py -b [your new backend version] -f [your new frontend version]
```

Run next command for help:
```bash
$ sudo ./upgrade_platform.py -h
```

After upgrade dorry platform, if dorry platform backend version and dorry platform frontend version changed on dorry-platform page, that means dorry market has been installed successfully.